/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

Object.defineProperty(exports, "__esModule", { value: true });
var MOVE;
(function (MOVE) {
    MOVE[MOVE["TOP"] = 0] = "TOP";
    MOVE[MOVE["BOTTOM"] = 1] = "BOTTOM";
    MOVE[MOVE["LEFT"] = 2] = "LEFT";
    MOVE[MOVE["RIGHT"] = 3] = "RIGHT";
    MOVE[MOVE["RESET"] = 4] = "RESET";
})(MOVE = exports.MOVE || (exports.MOVE = {}));
class Tile {
    constructor(value = 0) {
        this._value = 0;
        this._hasValueDoubled = false;
        this._element = undefined;
        this._location2d = {
            x: 0,
            y: 0
        };
        this._value = value;
        this._createElement();
    }
    generateValue() {
        this._value = Tile.INITIAL_VALUE_CANDIDATES[Math.round(Math.random())];
    }
    getHasValueDoubled() { return this._hasValueDoubled; }
    getElement() { return this._element; }
    getValue() { return this._value; }
    setValue(value) {
        this._value = value;
        this._hasValueDoubled = false;
    }
    clearValue() { this.setValue(0); }
    doubleValue() {
        this._value *= 2;
        this._hasValueDoubled = true;
        return;
    }
    setLocation2d(x, y) {
        this._location2d.x = x;
        this._location2d.y = y;
        return;
    }
    destructor() {
        this._value = 0;
        this._hasValueDoubled = false;
        this._location2d.x = 0;
        this._location2d.y = 0;
        this.render();
        return;
    }
    async viewRenderMove() {
        this._setTransitionDuration(Tile.TIMING.MOVE);
        this._setTranslate2d(this._location2d.x, this._location2d.y);
        await new Promise((fulfill) => {
            return setTimeout(fulfill, Tile.TIMING.MOVE);
        });
        this._setTransitionDuration(0);
        return;
    }
    render() {
        const strValue = this._value.toString();
        this._element.setAttribute('data-value', strValue);
        this._element.innerText = strValue;
        this._element.style.transform = '';
        if (this._value) {
            this._element.classList.add('is-visible');
        }
        else {
            this._element.classList.remove('is-visible');
        }
        return;
    }
    async renderPulse() {
        this._setAnimationDuration(Tile.TIMING.PULSE);
        this._element.classList.add('pulse');
        await new Promise((fulfill) => {
            return setTimeout(fulfill, Tile.TIMING.PULSE);
        });
        this._setAnimationDuration(0);
        this._element.classList.remove('pulse');
        return;
    }
    renderReset() {
        this._hasValueDoubled = false;
        this._location2d.x = 0;
        this._location2d.y = 0;
        this._setTranslate2d();
        return;
    }
    _createElement() {
        if (this._element) {
            return;
        }
        this._element = document.createElement('div');
        this._element.classList.add('tile');
        return;
    }
    _setTranslate2d(x = 0, y = 0) {
        if (x || y) {
            this._element
                .style
                .transform = `translate3d(${x}px, ${y}px, 0)`;
        }
        else {
            this._element.style.transform = '';
        }
        return;
    }
    _setTransitionDuration(ms) {
        if (ms) {
            this._element.style.transitionDuration = `${ms / 1000}s`;
        }
        else {
            this._element.style.transitionDuration = '';
        }
        return;
    }
    _setAnimationDuration(ms) {
        if (ms) {
            this._element.style.animationDuration = `${ms / 1000}s`;
        }
        else {
            this._element.style.animationDuration = '';
        }
    }
}
Tile.INITIAL_VALUE_CANDIDATES = [2, 4];
Tile.TIMING = {
    MOVE: 100,
    PULSE: 100,
    DROP_IN: 2000,
};
Tile.EVENT = {
    TRANSITION_END: 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend'
};
exports.default = Tile;


/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

Object.defineProperty(exports, "__esModule", { value: true });
const Tile_1 = __webpack_require__(0);
const Controls_1 = __webpack_require__(3);
const Grid_1 = __webpack_require__(4);
class Main {
    constructor() {
        this._controls = new Controls_1.default();
        this._grid = new Grid_1.default(4);
        this._controlsEventListenerBound = this._controlsEventlistener.bind(this);
        this._controls.registerListener(Tile_1.MOVE.TOP, this._controlsEventListenerBound);
        this._controls.registerListener(Tile_1.MOVE.BOTTOM, this._controlsEventListenerBound);
        this._controls.registerListener(Tile_1.MOVE.LEFT, this._controlsEventListenerBound);
        this._controls.registerListener(Tile_1.MOVE.RIGHT, this._controlsEventListenerBound);
        this._controls.registerListener(Tile_1.MOVE.RESET, this._controlsEventListenerBound);
        this._grid.init();
    }
    _controlsEventlistener(move) {
        if (move == Tile_1.MOVE.RESET) {
            this._grid.reset();
        }
        else {
            this._grid.move(move);
        }
        return;
    }
}
const main = new Main();


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

Object.defineProperty(exports, "__esModule", { value: true });
const Tile_1 = __webpack_require__(0);
var KEY;
(function (KEY) {
    KEY[KEY["LEFT"] = 37] = "LEFT";
    KEY[KEY["UP"] = 38] = "UP";
    KEY[KEY["RIGHT"] = 39] = "RIGHT";
    KEY[KEY["DOWN"] = 40] = "DOWN";
    KEY[KEY["R"] = 82] = "R";
})(KEY || (KEY = {}));
class Controls {
    constructor() {
        this._listeners = {};
        this._initKeyboardControls();
        this._initGestureControls();
        this._initViewControls();
    }
    registerListener(move, listener) {
        if (!this._listeners[move])
            this._listeners[move] = [];
        this._listeners[move].push(listener);
        return;
    }
    _initViewControls() {
        this._elementNewGame = document.getElementById('btn-new-game');
        this._elementNewGame.addEventListener('click', this._callListeners.bind(this, Tile_1.MOVE.RESET));
        return;
    }
    _initKeyboardControls() {
        this._keyboardEventBound = this._keyboardEvent.bind(this);
        document.addEventListener(Controls.KEY_EVENT, this._keyboardEventBound);
        return;
    }
    _initGestureControls() {
        const element = document.getElementById('grid');
        if (!element)
            return;
        const touchEvents = window.navigator.msPointerEnabled
            ? Controls.TOUCH_EVENT.MS
            : Controls.TOUCH_EVENT;
        let startX = 0;
        let startY = 0;
        element.addEventListener(touchEvents.START, (event) => {
            if (window.navigator.msPointerEnabled) {
                startX = event.pageX;
                startY = event.pageY;
            }
            else {
                startX = event.touches[0].clientX;
                startY = event.touches[0].clientY;
            }
            event.preventDefault();
            return;
        });
        element.addEventListener(touchEvents.MOVE, (event) => {
            event.preventDefault();
            return;
        });
        element.addEventListener(touchEvents.END, (event) => {
            let endX = 0;
            let endY = 0;
            if (window.navigator.msPointerEnabled) {
                endX = event.pageX;
                endY = event.pageY;
            }
            else {
                endX = event.changedTouches[0].clientX;
                endY = event.changedTouches[0].clientY;
            }
            const diffX = startX - endX;
            const diffY = startY - endY;
            const absDiffX = Math.abs(diffX);
            const absDiffY = Math.abs(diffY);
            if (Math.max(absDiffX, absDiffY) > 10) {
                this._callListeners(absDiffX > absDiffY
                    ? diffX > 0 ? Tile_1.MOVE.LEFT : Tile_1.MOVE.RIGHT
                    : diffY > 0 ? Tile_1.MOVE.TOP : Tile_1.MOVE.BOTTOM);
            }
            return;
        });
        return;
    }
    _keyboardEvent(event) {
        const hasModifier = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey;
        if (hasModifier)
            return;
        switch (event.which) {
            case KEY.DOWN:
                this._callListeners(Tile_1.MOVE.BOTTOM);
                break;
            case KEY.UP:
                this._callListeners(Tile_1.MOVE.TOP);
                break;
            case KEY.LEFT:
                this._callListeners(Tile_1.MOVE.LEFT);
                break;
            case KEY.RIGHT:
                this._callListeners(Tile_1.MOVE.RIGHT);
                break;
            case KEY.R:
                this._callListeners(Tile_1.MOVE.RESET);
                break;
        }
        return;
    }
    _callListeners(move) {
        if (this._listeners[move]) {
            this._listeners[move].forEach((listener) => {
                listener(move);
                return;
            });
        }
        return;
    }
}
Controls.KEY_EVENT = 'keydown';
Controls.TOUCH_EVENT = {
    START: 'touchstart',
    END: 'touchend',
    MOVE: 'touchmove',
    MS: {
        START: 'MSPoinerDown',
        END: 'MSPointerMove',
        MOVE: 'MSPointerUp',
    }
};
exports.default = Controls;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

Object.defineProperty(exports, "__esModule", { value: true });
const Slot_1 = __webpack_require__(5);
const Tile_1 = __webpack_require__(0);
const Score_1 = __webpack_require__(6);
var PLAY_STATE;
(function (PLAY_STATE) {
    PLAY_STATE[PLAY_STATE["ONGOIGN"] = 0] = "ONGOIGN";
    PLAY_STATE[PLAY_STATE["WIN"] = 1] = "WIN";
    PLAY_STATE[PLAY_STATE["LOST"] = 2] = "LOST";
})(PLAY_STATE || (PLAY_STATE = {}));
class Grid {
    constructor(gridSize) {
        this._isLocked = false;
        this._hasUpdatedOnLastMove = false;
        this._gridSize = 4;
        this._matrix = [];
        this._idxSlotsEmpty = new Set();
        this._idxSlotsOccupied = new Set();
        this._gridSize = gridSize;
        Slot_1.default.ElGrid = this.constructor.ElGrid;
        Slot_1.default.GridSize = this._gridSize;
        this._score = new Score_1.default();
    }
    static _ToArray(iterator) {
        const arr = [];
        let next = iterator.next();
        do {
            if (typeof next.value != 'undefined')
                arr.push(next.value);
            next = iterator.next();
        } while (!next.done);
        return arr;
    }
    init() {
        this._matrix.length = this._gridSize * this._gridSize;
        this._matrix = this._matrix
            .fill(undefined)
            .map((value, idx) => {
            this._idxSlotsEmpty.add(idx);
            return new Slot_1.default(idx);
        });
        this.generateTile();
        this.generateTile();
    }
    async move(move) {
        if (this._isLocked)
            return;
        const concurrentExecution = [];
        this._hasUpdatedOnLastMove = false;
        for (let idx = 0, l = this._matrix.length; idx < l; idx += this._gridSize + 1) {
            concurrentExecution.push(this.moveVector.call(this, idx, move));
        }
        await Promise.all(concurrentExecution);
        this._updatePlayState();
        if (this._hasUpdatedOnLastMove)
            this.generateTile();
        this._Print();
        return;
    }
    reset() {
        this._score.reset();
        this._score.viewRender();
        this._matrix
            .forEach((slot, idx) => {
            this._idxSlotsEmpty.add(idx);
            this._idxSlotsOccupied.delete(idx);
            slot.getTile().destructor();
        });
        this._updatePlayState();
        this.generateTile();
        this.generateTile();
        return;
    }
    async moveVector(idx, move) {
        const vectorIndices = this.getMoveVectorIndices(idx, move);
        const vector = this.getMoveVector(idx, move);
        let hasIterateeFired = false;
        return new Promise((fulfill) => {
            this.calcMoveResult(vector, async (slot, targetSlot) => {
                hasIterateeFired = true;
                this._hasUpdatedOnLastMove = true;
                await this.moveIteratee(slot, targetSlot, move);
                return fulfill();
            });
            if (!hasIterateeFired) {
                return fulfill();
            }
        });
    }
    async moveIteratee(slot, targetSlot, move) {
        if (slot == targetSlot)
            return;
        const tile = slot.getTile();
        const targetTile = targetSlot.getTile();
        this._idxSlotsOccupied.add(targetSlot.getIdx());
        this._idxSlotsOccupied.delete(slot.getIdx());
        this._idxSlotsEmpty.add(slot.getIdx());
        this._idxSlotsEmpty.delete(targetSlot.getIdx());
        const location2d = this.viewCalcLocation2d(slot, targetSlot, move);
        tile.setLocation2d(location2d.x, location2d.y);
        await tile.viewRenderMove();
        if (tile.getValue()) {
            tile.renderReset();
        }
        else {
            tile.destructor();
        }
        targetTile.render();
        if (targetTile.getHasValueDoubled()) {
            targetTile.renderPulse();
            this._score.add(targetTile.getValue());
            this._score.viewRender();
        }
        return;
    }
    generateTile() {
        if (this._isLocked)
            return;
        const size = this._idxSlotsEmpty.size;
        if (!size)
            return;
        const slots = Grid._ToArray(this._idxSlotsEmpty.values());
        const randomIdx = Math.round(Math.random() * (size - 1));
        const slotIdx = slots[randomIdx];
        this._matrix[slotIdx].getTile().generateValue();
        this._matrix[slotIdx].getTile().render();
        this._idxSlotsEmpty.delete(slotIdx);
        this._idxSlotsOccupied.add(slotIdx);
        return;
    }
    calcMoveResult(vector, iteratee = undefined) {
        for (let i = 0, l = this._gridSize - 1; i < l; i++) {
            const slot = vector[i];
            let j = i + 1;
            let isPairFound = false;
            while (!isPairFound && j < this._gridSize) {
                const nextSlot = vector[j];
                if (slot.getTile().getValue()) {
                    if (nextSlot.getTile().getValue() == slot.getTile().getValue()) {
                        slot.getTile().doubleValue();
                        nextSlot.getTile().clearValue();
                        isPairFound = true;
                        if (iteratee)
                            iteratee(nextSlot, slot);
                    }
                    else if (nextSlot.getTile().getValue()) {
                        isPairFound = true;
                    }
                }
                else if (nextSlot.getTile().getValue()) {
                    slot.getTile().setValue(nextSlot.getTile().getValue());
                    nextSlot.getTile().clearValue();
                    if (iteratee)
                        iteratee(nextSlot, slot);
                }
                j++;
            }
        }
        return vector;
    }
    getMoveVector(idx, move) {
        const vectorIndices = this.getMoveVectorIndices(idx, move);
        const vector = vectorIndices.map(idx => this._matrix[idx]);
        return vector;
    }
    getMoveVectorIndices(idx, move) {
        const length = this._matrix.length;
        const column = idx % this._gridSize;
        const row = Math.floor(idx / this._gridSize);
        const vector = [];
        switch (move) {
            case (Tile_1.MOVE.TOP):
                idx = column;
                break;
            case (Tile_1.MOVE.BOTTOM):
                idx = length - (this._gridSize - column);
                break;
            case (Tile_1.MOVE.LEFT):
                idx = row * this._gridSize;
                break;
            case (Tile_1.MOVE.RIGHT):
                idx = row * this._gridSize + (this._gridSize - 1);
                break;
        }
        for (let i = 0; i < this._gridSize; i++) {
            vector.push(idx);
            switch (move) {
                case (Tile_1.MOVE.TOP):
                    idx += this._gridSize;
                    break;
                case (Tile_1.MOVE.BOTTOM):
                    idx -= this._gridSize;
                    break;
                case (Tile_1.MOVE.LEFT):
                    idx++;
                    break;
                case (Tile_1.MOVE.RIGHT):
                    idx--;
                    break;
            }
        }
        return vector;
    }
    viewCalcLocation2d(slot, targetSlot, move) {
        const vectorIndices = this.getMoveVectorIndices(slot.getIdx(), move);
        const idxDiff = Math.abs(vectorIndices.indexOf(slot.getIdx()) - vectorIndices.indexOf(targetSlot.getIdx()));
        const clientSlotSize = slot.viewGetClientWidth();
        const location2d = {
            x: 0,
            y: 0
        };
        switch (move) {
            case Tile_1.MOVE.TOP:
                location2d.y = idxDiff * clientSlotSize * -1;
                break;
            case Tile_1.MOVE.BOTTOM:
                location2d.y = idxDiff * clientSlotSize;
                break;
            case Tile_1.MOVE.LEFT:
                location2d.x = idxDiff * clientSlotSize * -1;
                break;
            case Tile_1.MOVE.RIGHT:
                location2d.x = idxDiff * clientSlotSize;
                break;
        }
        return location2d;
    }
    _viewRenderPlayState(state) {
        switch (state) {
            case PLAY_STATE.ONGOIGN:
                this._isLocked = false;
                Grid.ElGrid.classList.remove('has-won');
                Grid.ElGrid.classList.remove('has-lost');
                break;
            case PLAY_STATE.WIN:
                this._isLocked = true;
                Grid.ElGrid.classList.remove('has-lost');
                Grid.ElGrid.classList.add('has-won');
                break;
            case PLAY_STATE.LOST:
                this._isLocked = true;
                Grid.ElGrid.classList.remove('has-won');
                Grid.ElGrid.classList.add('has-lost');
                break;
        }
        return;
    }
    _updatePlayState() {
        const slotOccupiedIndices = Grid._ToArray(this._idxSlotsOccupied.values());
        let hasWon = false;
        let i = 0;
        while (!hasWon && i < slotOccupiedIndices.length) {
            const slot = this._matrix[slotOccupiedIndices[i]];
            hasWon = slot.getTile().getValue() == Grid._WIN_VALUE;
            i++;
        }
        if (hasWon) {
            this._isLocked = true;
            this._viewRenderPlayState(PLAY_STATE.WIN);
        }
        else if (!this._idxSlotsEmpty.size) {
            this._isLocked = true;
            this._viewRenderPlayState(PLAY_STATE.LOST);
        }
        else if (this._isLocked) {
            this._isLocked = false;
            this._viewRenderPlayState(PLAY_STATE.ONGOIGN);
        }
        return;
    }
    _Print() {
        const formattedValues = [];
        for (let i = 0; i < this._matrix.length; i++) {
            const value = this._matrix[i].getTile().getValue().toString();
            formattedValues.push('     '.substring(0, 5 - value.length) + value);
            if ((i + 1) % this._gridSize == 0) {
                formattedValues.push('\n');
            }
        }
        console.log(formattedValues.join(''));
    }
}
Grid._WIN_VALUE = 2048;
Grid.ElGrid = document.querySelector('#grid');
exports.default = Grid;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

Object.defineProperty(exports, "__esModule", { value: true });
const Tile_1 = __webpack_require__(0);
class Slot {
    constructor(idx, tile = undefined) {
        this._tile = undefined;
        this._idx = -1;
        this._element = undefined;
        this._idx = idx;
        this._tile = tile || new Tile_1.default();
        this._createElement();
    }
    getIdx() { return this._idx; }
    getTile() { return this._tile; }
    viewGetClientWidth() { return this._element.clientWidth; }
    viewGetClientHeight() { return this._element.clientHeight; }
    getElement() { return this._element; }
    _createElement() {
        if (this._element) {
            return;
        }
        this._element = document.createElement('div');
        this._element.classList.add('slot');
        this._element.style.width = `${Math.floor(100 / Slot.GridSize)}%`;
        this._element.insertBefore(this._tile.getElement(), null);
        Slot.ElGrid.insertBefore(this._element, null);
        return;
    }
}
Slot.GridSize = 0;
Slot.ElGrid = undefined;
exports.default = Slot;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

Object.defineProperty(exports, "__esModule", { value: true });
class Score {
    constructor(value = 0) {
        this._value = 0;
        this._value = value;
        this._element = document.getElementById('score');
    }
    add(value = 0) {
        this._value += value;
        return;
    }
    reset() {
        this._value = 0;
        return;
    }
    viewRender() {
        if (this._element)
            this._element.innerHTML = this._value.toString();
        return;
    }
}
exports.default = Score;


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgN2YwZTE0M2FmNzY1MjFjYmMyYTYiLCJ3ZWJwYWNrOi8vLy4vU3JjL1NjcmlwdHMvVGlsZS50cyIsIndlYnBhY2s6Ly8vLi9TcmMvU2NyaXB0cy9NYWluLnRzIiwid2VicGFjazovLy8uL1NyYy9TY3JpcHRzL0NvbnRyb2xzLnRzIiwid2VicGFjazovLy8uL1NyYy9TY3JpcHRzL0dyaWQudHMiLCJ3ZWJwYWNrOi8vLy4vU3JjL1NjcmlwdHMvU2xvdC50cyIsIndlYnBhY2s6Ly8vLi9TcmMvU2NyaXB0cy9TY29yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDeERBLElBQVksSUFNWDtBQU5ELFdBQVksSUFBSTtJQUNkLDZCQUFHO0lBQ0gsbUNBQU07SUFDTiwrQkFBSTtJQUNKLGlDQUFLO0lBQ0wsaUNBQUs7QUFDUCxDQUFDLEVBTlcsSUFBSSxHQUFKLFlBQUksS0FBSixZQUFJLFFBTWY7QUFFRDtJQW1CRSxZQUFtQixRQUFnQixDQUFDO1FBUjVCLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFDbkIscUJBQWdCLEdBQVksS0FBSyxDQUFDO1FBQ2xDLGFBQVEsR0FBZ0IsU0FBUyxDQUFDO1FBQ2xDLGdCQUFXLEdBQUc7WUFDcEIsQ0FBQyxFQUFFLENBQUM7WUFDSixDQUFDLEVBQUUsQ0FBQztTQUNMO1FBR0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTSxhQUFhO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRU0sa0JBQWtCLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFFdEQsVUFBVSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUN0QyxRQUFRLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLFFBQVEsQ0FBQyxLQUFhO1FBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVNLFVBQVUsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsQyxXQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVNLGFBQWEsQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUN2QyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXZCLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTSxVQUFVO1FBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVkLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTSxLQUFLLENBQUMsY0FBYztRQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFN0QsTUFBTSxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQzVCLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFL0IsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVNLE1BQU07UUFDWCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXhDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUVuQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQy9DLENBQUM7UUFFRCxNQUFNLENBQUM7SUFDVCxDQUFDO0lBRU0sS0FBSyxDQUFDLFdBQVc7UUFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXJDLE1BQU0sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUM1QixNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV4QyxNQUFNLENBQUM7SUFDVCxDQUFDO0lBRU0sV0FBVztRQUNoQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTyxjQUFjO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXBDLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTyxlQUFlLENBQUMsSUFBWSxDQUFDLEVBQUUsSUFBWSxDQUFDO1FBQ2xELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ1gsSUFBSSxDQUFDLFFBQVE7aUJBQ1YsS0FBSztpQkFDTCxTQUFTLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7UUFDbEQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNyQyxDQUFDO1FBRUQsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLHNCQUFzQixDQUFDLEVBQVU7UUFDdkMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLEdBQUcsRUFBRSxHQUFHLElBQUksR0FBRyxDQUFDO1FBQzNELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM5QyxDQUFDO1FBRUQsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLHFCQUFxQixDQUFDLEVBQVU7UUFDdEMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLEdBQUcsRUFBRSxHQUFHLElBQUksR0FBRyxDQUFDO1FBQzFELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM3QyxDQUFDO0lBQ0gsQ0FBQzs7QUF0SnVCLDZCQUF3QixHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ2xDLFdBQU0sR0FBRztJQUMvQixJQUFJLEVBQUUsR0FBRztJQUNULEtBQUssRUFBRSxHQUFHO0lBQ1YsT0FBTyxFQUFFLElBQUk7Q0FDZDtBQUN1QixVQUFLLEdBQUc7SUFDOUIsY0FBYyxFQUFFLGlGQUFpRjtDQUNsRztBQWlKSCxrQkFBZSxJQUFJLENBQUM7Ozs7Ozs7OztBQ3ZLcEIsc0NBQThCO0FBQzlCLDBDQUFrQztBQUNsQyxzQ0FBMEI7QUFFMUI7SUFNRTtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxrQkFBUSxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGNBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV6QixJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUcxRSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLFdBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsV0FBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLFdBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBRzlFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVPLHNCQUFzQixDQUFDLElBQVk7UUFDekMsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLFdBQUksQ0FBQyxLQUFLLENBQUMsRUFBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFRCxNQUFNLENBQUM7SUFDVCxDQUFDO0NBQ0Y7QUFFRCxNQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDOzs7Ozs7OztBQ3RDeEIsc0NBQThCO0FBVTlCLElBQUssR0FNSjtBQU5ELFdBQUssR0FBRztJQUNOLDhCQUFTO0lBQ1QsMEJBQU87SUFDUCxnQ0FBVTtJQUNWLDhCQUFTO0lBQ1Qsd0JBQU07QUFDUixDQUFDLEVBTkksR0FBRyxLQUFILEdBQUcsUUFNUDtBQUVEO0lBbUJFO1FBSlEsZUFBVSxHQUFlLEVBQUUsQ0FBQztRQUtsQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU0sZ0JBQWdCLENBQUMsSUFBWSxFQUFFLFFBQVE7UUFDNUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDdkQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckMsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLGlCQUFpQjtRQUN2QixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFdBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzNGLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTyxxQkFBcUI7UUFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTFELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3hFLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTyxvQkFBb0I7UUFDMUIsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVoRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUVyQixNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLGdCQUFnQjtZQUNuRCxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3pCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVztRQUV4QixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDZixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFZixPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQWlCLEVBQUUsRUFBRTtZQUNoRSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxHQUFJLEtBQWEsQ0FBQyxLQUFLLENBQUM7Z0JBQzlCLE1BQU0sR0FBSSxLQUFhLENBQUMsS0FBSyxDQUFDO1lBQ2hDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQ2xDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNwQyxDQUFDO1lBRUQsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQztRQUNULENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNuRCxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsTUFBTSxDQUFDO1FBQ1QsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQWlCLEVBQUUsRUFBRTtZQUM5RCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUM7WUFDYixJQUFJLElBQUksR0FBRyxDQUFDLENBQUM7WUFFYixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDdEMsSUFBSSxHQUFJLEtBQWEsQ0FBQyxLQUFLLENBQUM7Z0JBQzVCLElBQUksR0FBSSxLQUFhLENBQUMsS0FBSyxDQUFDO1lBQzlCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixJQUFJLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQ3ZDLElBQUksR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUN6QyxDQUFDO1lBRUQsTUFBTSxLQUFLLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQztZQUM1QixNQUFNLEtBQUssR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBRTVCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakMsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVqQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxRQUFRO29CQUNyQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBSSxDQUFDLEtBQUs7b0JBQ3BDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxXQUFJLENBQUMsTUFBTSxDQUNyQyxDQUFDO1lBQ0osQ0FBQztZQUVELE1BQU0sQ0FBQztRQUNULENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLGNBQWMsQ0FBQyxLQUFvQjtRQUN6QyxNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsUUFBUTtRQUdwRixFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFDZCxNQUFNLENBQUM7UUFFVCxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNwQixLQUFLLEdBQUcsQ0FBQyxJQUFJO2dCQUNYLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNqQyxLQUFLLENBQUM7WUFDUixLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUNULElBQUksQ0FBQyxjQUFjLENBQUMsV0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5QixLQUFLLENBQUM7WUFDUixLQUFLLEdBQUcsQ0FBQyxJQUFJO2dCQUNYLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQixLQUFLLENBQUM7WUFDUixLQUFLLEdBQUcsQ0FBQyxLQUFLO2dCQUNaLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLENBQUM7WUFDUixLQUFLLEdBQUcsQ0FBQyxDQUFDO2dCQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLENBQUM7UUFDVixDQUFDO1FBRUQsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLGNBQWMsQ0FBQyxJQUFZO1FBQ2pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixNQUFNLENBQUM7WUFDVCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUM7SUFDVCxDQUFDOztBQTdJdUIsa0JBQVMsR0FBRyxTQUFTLENBQUM7QUFDdEIsb0JBQVcsR0FBRztJQUNwQyxLQUFLLEVBQUUsWUFBWTtJQUNuQixHQUFHLEVBQUUsVUFBVTtJQUNmLElBQUksRUFBRSxXQUFXO0lBRWpCLEVBQUUsRUFBRTtRQUNGLEtBQUssRUFBRSxjQUFjO1FBQ3JCLEdBQUcsRUFBRSxlQUFlO1FBQ3BCLElBQUksRUFBRSxhQUFhO0tBQ3BCO0NBQ0Y7QUFxSUgsa0JBQWUsUUFBUSxDQUFDOzs7Ozs7OztBQ25LeEIsc0NBQTBCO0FBQzFCLHNDQUFpRDtBQUNqRCx1Q0FBNEI7QUFNNUIsSUFBSyxVQUlKO0FBSkQsV0FBSyxVQUFVO0lBQ2IsaURBQU87SUFDUCx5Q0FBRztJQUNILDJDQUFJO0FBQ04sQ0FBQyxFQUpJLFVBQVUsS0FBVixVQUFVLFFBSWQ7QUFFRDtJQXlCRSxZQUFtQixRQUFnQjtRQXBCM0IsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQiwwQkFBcUIsR0FBWSxLQUFLLENBQUM7UUFDdkMsY0FBUyxHQUFXLENBQUMsQ0FBQztRQUN0QixZQUFPLEdBQVcsRUFBRSxDQUFDO1FBQ3JCLG1CQUFjLEdBQWdCLElBQUksR0FBRyxFQUFFLENBQUM7UUFDeEMsc0JBQWlCLEdBQWdCLElBQUksR0FBRyxFQUFFLENBQUM7UUFnQmpELElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBRTFCLGNBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDdEMsY0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBRS9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFLLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBbkJPLE1BQU0sQ0FBQyxRQUFRLENBQUksUUFBcUI7UUFDOUMsTUFBTSxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxJQUFJLEdBQXNCLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUU5QyxHQUFHLENBQUM7WUFDRixFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLElBQUksV0FBVyxDQUFDO2dCQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNELElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDekIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtRQUVyQixNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQVdNLElBQUk7UUFDVCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFHdEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTzthQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDO2FBQ2YsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdCLE1BQU0sQ0FBQyxJQUFJLGNBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBWTtRQUM1QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBRTNCLE1BQU0sbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBRS9CLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFHbkMsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRSxDQUFDO1lBRTlFLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDbEUsQ0FBQztRQUdELE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBRXZDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBR3hCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVwRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFZCxNQUFNLENBQUM7SUFDVCxDQUFDO0lBRU0sS0FBSztRQUdWLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUd6QixJQUFJLENBQUMsT0FBTzthQUNULE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUNyQixJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRW5DLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztRQUdMLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBR3hCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFcEIsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVTLEtBQUssQ0FBQyxVQUFVLENBQUMsR0FBVyxFQUFFLElBQVk7UUFDbEQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUU3QyxJQUFJLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUU3QixNQUFNLENBQUMsSUFBSSxPQUFPLENBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxFQUFFO2dCQUNyRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7Z0JBQ2xDLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNoRCxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkIsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ25CLENBQUM7UUFDSCxDQUFDLENBQUM7SUFDSixDQUFDO0lBRVMsS0FBSyxDQUFDLFlBQVksQ0FDMUIsSUFBVSxFQUNWLFVBQWdCLEVBQ2hCLElBQVk7UUFHWixFQUFFLENBQUMsQ0FBQyxJQUFJLElBQUksVUFBVSxDQUFDO1lBQ3JCLE1BQU0sQ0FBQztRQUVULE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM1QixNQUFNLFVBQVUsR0FBRyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUM7UUFHeEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBR2hELE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFL0MsTUFBTSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDNUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDckIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3BCLENBQUM7UUFFRCxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFcEIsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUV6QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUN2QyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzNCLENBQUM7UUFFRCxNQUFNLENBQUM7SUFDVCxDQUFDO0lBRVMsWUFBWTtRQUNwQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBRTNCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1FBRXRDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBRWxCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzFELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekQsTUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBR2pDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUd6QyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXBDLE1BQU0sQ0FBQztJQUNULENBQUM7SUFFTSxjQUFjLENBQ25CLE1BQWMsRUFDZCxXQUFnQyxTQUFTO1FBRXpDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ25ELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV2QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBRXhCLE9BQU0sQ0FBQyxXQUFXLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDekMsTUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUczQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQzt3QkFDL0QsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUM3QixRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBQ2hDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0JBRW5CLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQzs0QkFBQyxRQUFRLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUN6QyxDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUN6QyxXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUNyQixDQUFDO2dCQUNILENBQUM7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3pDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7b0JBQ3ZELFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztvQkFFaEMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDO3dCQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3pDLENBQUM7Z0JBRUQsQ0FBQyxFQUFFLENBQUM7WUFDTixDQUFDO1FBQ0gsQ0FBQztRQUVELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVTLGFBQWEsQ0FBQyxHQUFXLEVBQUUsSUFBWTtRQUMvQyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNELE1BQU0sTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFM0QsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRVMsb0JBQW9CLENBQUMsR0FBVyxFQUFFLElBQVk7UUFDdEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDbkMsTUFBTSxNQUFNLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDcEMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRTdDLE1BQU0sTUFBTSxHQUFhLEVBQUUsQ0FBQztRQUc1QixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2IsS0FBSyxDQUFDLFdBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQ2IsR0FBRyxHQUFHLE1BQU0sQ0FBQztnQkFDYixLQUFLLENBQUM7WUFDUixLQUFLLENBQUMsV0FBSSxDQUFDLE1BQU0sQ0FBQztnQkFDaEIsR0FBRyxHQUFHLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQ3pDLEtBQUssQ0FBQztZQUNSLEtBQUssQ0FBQyxXQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNkLEdBQUcsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDM0IsS0FBSyxDQUFDO1lBQ1IsS0FBSyxDQUFDLFdBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ2YsR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbEQsS0FBSyxDQUFDO1FBQ1YsQ0FBQztRQUVELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFHakIsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDYixLQUFLLENBQUMsV0FBSSxDQUFDLEdBQUcsQ0FBQztvQkFDYixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDdEIsS0FBSyxDQUFDO2dCQUNSLEtBQUssQ0FBQyxXQUFJLENBQUMsTUFBTSxDQUFDO29CQUNoQixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDdEIsS0FBSyxDQUFDO2dCQUNSLEtBQUssQ0FBQyxXQUFJLENBQUMsSUFBSSxDQUFDO29CQUNkLEdBQUcsRUFBRSxDQUFDO29CQUNOLEtBQUssQ0FBQztnQkFDUixLQUFLLENBQUMsV0FBSSxDQUFDLEtBQUssQ0FBQztvQkFDZixHQUFHLEVBQUUsQ0FBQztvQkFDTixLQUFLLENBQUM7WUFDVixDQUFDO1FBQ0gsQ0FBQztRQUVELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVTLGtCQUFrQixDQUFDLElBQVUsRUFBRSxVQUFnQixFQUFFLElBQVk7UUFDckUsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRSxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzVHLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQ2pELE1BQU0sVUFBVSxHQUFnQjtZQUM5QixDQUFDLEVBQUUsQ0FBQztZQUNKLENBQUMsRUFBRSxDQUFDO1NBQ0w7UUFFRCxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2IsS0FBSyxXQUFJLENBQUMsR0FBRztnQkFDWCxVQUFVLENBQUMsQ0FBQyxHQUFHLE9BQU8sR0FBRyxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLEtBQUssQ0FBQztZQUNSLEtBQUssV0FBSSxDQUFDLE1BQU07Z0JBQ2QsVUFBVSxDQUFDLENBQUMsR0FBRyxPQUFPLEdBQUcsY0FBYyxDQUFDO2dCQUN4QyxLQUFLLENBQUM7WUFDUixLQUFLLFdBQUksQ0FBQyxJQUFJO2dCQUNaLFVBQVUsQ0FBQyxDQUFDLEdBQUcsT0FBTyxHQUFHLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsS0FBSyxDQUFDO1lBQ1IsS0FBSyxXQUFJLENBQUMsS0FBSztnQkFDYixVQUFVLENBQUMsQ0FBQyxHQUFHLE9BQU8sR0FBRyxjQUFjLENBQUM7Z0JBQ3hDLEtBQUssQ0FBQztRQUNWLENBQUM7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFFTyxvQkFBb0IsQ0FBQyxLQUFhO1FBQ3hDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDZCxLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3pDLEtBQUssQ0FBQztZQUNSLEtBQUssVUFBVSxDQUFDLEdBQUc7Z0JBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDckMsS0FBSyxDQUFDO1lBQ1IsS0FBSyxVQUFVLENBQUMsSUFBSTtnQkFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN0QyxLQUFLLENBQUM7UUFDVixDQUFDO1FBRUQsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLGdCQUFnQjtRQUN0QixNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFFM0UsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVWLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxHQUFHLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2pELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDdEQsQ0FBQyxFQUFFLENBQUM7UUFDTixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBRUQsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVPLE1BQU07UUFDWixNQUFNLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFFM0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDOUQsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBRXJFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QixDQUFDO1FBQ0gsQ0FBQztRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7O0FBMVd1QixlQUFVLEdBQUcsSUFBSSxDQUFDO0FBRTVCLFdBQU0sR0FBZ0IsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQWdCLENBQUM7QUFnWHJGLGtCQUFlLElBQUksQ0FBQzs7Ozs7Ozs7QUNqWXBCLHNDQUEwQjtBQUUxQjtJQVFFLFlBQW1CLEdBQVcsRUFBRSxPQUFhLFNBQVM7UUFKOUMsVUFBSyxHQUFTLFNBQVMsQ0FBQztRQUN4QixTQUFJLEdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbEIsYUFBUSxHQUFnQixTQUFTLENBQUM7UUFHeEMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLElBQUksSUFBSSxjQUFJLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVNLE1BQU0sS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDOUIsT0FBTyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNoQyxrQkFBa0IsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQzFELG1CQUFtQixLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFFNUQsVUFBVSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUVyQyxjQUFjO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO1FBR2xFLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFHMUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUU5QyxNQUFNLENBQUM7SUFDVCxDQUFDOztBQXBDYSxhQUFRLEdBQUcsQ0FBQyxDQUFDO0FBQ2IsV0FBTSxHQUFnQixTQUFTLENBQUM7QUFzQ2hELGtCQUFlLElBQUksQ0FBQzs7Ozs7Ozs7QUMxQ3BCO0lBSUUsWUFBbUIsUUFBZ0IsQ0FBQztRQUY1QixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBR3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sR0FBRyxDQUFDLFFBQWdCLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUM7UUFDckIsTUFBTSxDQUFDO0lBQ1QsQ0FBQztJQUVNLEtBQUs7UUFDVixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNoQixNQUFNLENBQUM7SUFDVCxDQUFDO0lBRU0sVUFBVTtRQUNmLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BFLE1BQU0sQ0FBQztJQUNULENBQUM7Q0FDRjtBQUVELGtCQUFlLEtBQUssQ0FBQyIsImZpbGUiOiJjbGllbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA3ZjBlMTQzYWY3NjUyMWNiYzJhNiIsImV4cG9ydCBpbnRlcmZhY2UgSUxvY2F0aW9uMmQge1xyXG4gIHg6IG51bWJlcixcclxuICB5OiBudW1iZXJcclxufVxyXG5cclxuZXhwb3J0IGVudW0gTU9WRSB7XHJcbiAgVE9QLFxyXG4gIEJPVFRPTSxcclxuICBMRUZULFxyXG4gIFJJR0hULFxyXG4gIFJFU0VUXHJcbn1cclxuXHJcbmNsYXNzIFRpbGUge1xyXG4gIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IElOSVRJQUxfVkFMVUVfQ0FORElEQVRFUyA9IFsyLCA0XTtcclxuICBwcml2YXRlIHN0YXRpYyByZWFkb25seSBUSU1JTkcgPSB7XHJcbiAgICBNT1ZFOiAxMDAsXHJcbiAgICBQVUxTRTogMTAwLFxyXG4gICAgRFJPUF9JTjogMjAwMCxcclxuICB9XHJcbiAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgRVZFTlQgPSB7XHJcbiAgICBUUkFOU0lUSU9OX0VORDogJ3dlYmtpdFRyYW5zaXRpb25FbmQgb3RyYW5zaXRpb25lbmQgb1RyYW5zaXRpb25FbmQgbXNUcmFuc2l0aW9uRW5kIHRyYW5zaXRpb25lbmQnXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF92YWx1ZTogbnVtYmVyID0gMDtcclxuICBwcml2YXRlIF9oYXNWYWx1ZURvdWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIF9lbGVtZW50OiBIVE1MRWxlbWVudCA9IHVuZGVmaW5lZDtcclxuICBwcml2YXRlIF9sb2NhdGlvbjJkID0ge1xyXG4gICAgeDogMCxcclxuICAgIHk6IDBcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcih2YWx1ZTogbnVtYmVyID0gMCkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcclxuICAgIHRoaXMuX2NyZWF0ZUVsZW1lbnQoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZW5lcmF0ZVZhbHVlKCkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSBUaWxlLklOSVRJQUxfVkFMVUVfQ0FORElEQVRFU1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkpXTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRIYXNWYWx1ZURvdWJsZWQoKSB7IHJldHVybiB0aGlzLl9oYXNWYWx1ZURvdWJsZWQ7IH1cclxuXHJcbiAgcHVibGljIGdldEVsZW1lbnQoKSB7IHJldHVybiB0aGlzLl9lbGVtZW50OyB9XHJcbiAgcHVibGljIGdldFZhbHVlKCkgeyByZXR1cm4gdGhpcy5fdmFsdWU7IH1cclxuICBwdWJsaWMgc2V0VmFsdWUodmFsdWU6IG51bWJlcikgeyBcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7IFxyXG4gICAgdGhpcy5faGFzVmFsdWVEb3VibGVkID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xlYXJWYWx1ZSgpIHsgdGhpcy5zZXRWYWx1ZSgwKTsgfVxyXG4gIHB1YmxpYyBkb3VibGVWYWx1ZSgpIHsgXHJcbiAgICB0aGlzLl92YWx1ZSAqPSAyOyBcclxuICAgIHRoaXMuX2hhc1ZhbHVlRG91YmxlZCA9IHRydWU7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0TG9jYXRpb24yZCh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgdGhpcy5fbG9jYXRpb24yZC54ID0geDtcclxuICAgIHRoaXMuX2xvY2F0aW9uMmQueSA9IHk7XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGRlc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IDA7XHJcbiAgICB0aGlzLl9oYXNWYWx1ZURvdWJsZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuX2xvY2F0aW9uMmQueCA9IDA7XHJcbiAgICB0aGlzLl9sb2NhdGlvbjJkLnkgPSAwO1xyXG4gICAgdGhpcy5yZW5kZXIoKTtcclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYXN5bmMgdmlld1JlbmRlck1vdmUoKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICB0aGlzLl9zZXRUcmFuc2l0aW9uRHVyYXRpb24oVGlsZS5USU1JTkcuTU9WRSk7XHJcbiAgICB0aGlzLl9zZXRUcmFuc2xhdGUyZCh0aGlzLl9sb2NhdGlvbjJkLngsIHRoaXMuX2xvY2F0aW9uMmQueSk7XHJcblxyXG4gICAgYXdhaXQgbmV3IFByb21pc2UoKGZ1bGZpbGwpID0+IHtcclxuICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVsZmlsbCwgVGlsZS5USU1JTkcuTU9WRSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLl9zZXRUcmFuc2l0aW9uRHVyYXRpb24oMCk7XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHJlbmRlcigpIHtcclxuICAgIGNvbnN0IHN0clZhbHVlID0gdGhpcy5fdmFsdWUudG9TdHJpbmcoKTtcclxuXHJcbiAgICB0aGlzLl9lbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS12YWx1ZScsIHN0clZhbHVlKTtcclxuICAgIHRoaXMuX2VsZW1lbnQuaW5uZXJUZXh0ID0gc3RyVmFsdWU7XHJcbiAgICB0aGlzLl9lbGVtZW50LnN0eWxlLnRyYW5zZm9ybSA9ICcnO1xyXG5cclxuICAgIGlmICh0aGlzLl92YWx1ZSkge1xyXG4gICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2lzLXZpc2libGUnKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnaXMtdmlzaWJsZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBhc3luYyByZW5kZXJQdWxzZSgpIHtcclxuICAgIHRoaXMuX3NldEFuaW1hdGlvbkR1cmF0aW9uKFRpbGUuVElNSU5HLlBVTFNFKTtcclxuICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZCgncHVsc2UnKTtcclxuXHJcbiAgICBhd2FpdCBuZXcgUHJvbWlzZSgoZnVsZmlsbCkgPT4ge1xyXG4gICAgICByZXR1cm4gc2V0VGltZW91dChmdWxmaWxsLCBUaWxlLlRJTUlORy5QVUxTRSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLl9zZXRBbmltYXRpb25EdXJhdGlvbigwKTtcclxuICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgncHVsc2UnKTtcclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcmVuZGVyUmVzZXQoKSB7XHJcbiAgICB0aGlzLl9oYXNWYWx1ZURvdWJsZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuX2xvY2F0aW9uMmQueCA9IDA7XHJcbiAgICB0aGlzLl9sb2NhdGlvbjJkLnkgPSAwO1xyXG4gICAgdGhpcy5fc2V0VHJhbnNsYXRlMmQoKTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2NyZWF0ZUVsZW1lbnQoKSB7XHJcbiAgICBpZiAodGhpcy5fZWxlbWVudCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5fZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKCd0aWxlJyk7XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfc2V0VHJhbnNsYXRlMmQoeDogbnVtYmVyID0gMCwgeTogbnVtYmVyID0gMCkge1xyXG4gICAgaWYgKHggfHwgeSkge1xyXG4gICAgICB0aGlzLl9lbGVtZW50XHJcbiAgICAgICAgLnN0eWxlXHJcbiAgICAgICAgLnRyYW5zZm9ybSA9IGB0cmFuc2xhdGUzZCgke3h9cHgsICR7eX1weCwgMClgO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS50cmFuc2Zvcm0gPSAnJztcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9zZXRUcmFuc2l0aW9uRHVyYXRpb24obXM6IG51bWJlcikge1xyXG4gICAgaWYgKG1zKSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gYCR7bXMgLyAxMDAwfXNgO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24gPSAnJztcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9zZXRBbmltYXRpb25EdXJhdGlvbihtczogbnVtYmVyKSB7XHJcbiAgICBpZiAobXMpIHtcclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5hbmltYXRpb25EdXJhdGlvbiA9IGAke21zIC8gMTAwMH1zYDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUuYW5pbWF0aW9uRHVyYXRpb24gPSAnJztcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRpbGU7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vU3JjL1NjcmlwdHMvVGlsZS50cyIsImltcG9ydCB7IE1PVkUgfSBmcm9tICcuL1RpbGUnO1xyXG5pbXBvcnQgQ29udHJvbHMgZnJvbSAnLi9Db250cm9scyc7XHJcbmltcG9ydCBHcmlkIGZyb20gJy4vR3JpZCc7XHJcblxyXG5jbGFzcyBNYWluIHtcclxuICBwcml2YXRlIF9jb250cm9sczogQ29udHJvbHM7XHJcbiAgcHJpdmF0ZSBfZ3JpZDogR3JpZDtcclxuXHJcbiAgcHJpdmF0ZSBfY29udHJvbHNFdmVudExpc3RlbmVyQm91bmQ7XHJcblxyXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuX2NvbnRyb2xzID0gbmV3IENvbnRyb2xzKCk7XHJcbiAgICB0aGlzLl9ncmlkID0gbmV3IEdyaWQoNCk7XHJcblxyXG4gICAgdGhpcy5fY29udHJvbHNFdmVudExpc3RlbmVyQm91bmQgPSB0aGlzLl9jb250cm9sc0V2ZW50bGlzdGVuZXIuYmluZCh0aGlzKTtcclxuXHJcbiAgICAvLyBpbml0IGNvbnRyb2xzIGV2ZW50IGxpc3Rlcm5zXHJcbiAgICB0aGlzLl9jb250cm9scy5yZWdpc3Rlckxpc3RlbmVyKE1PVkUuVE9QLCB0aGlzLl9jb250cm9sc0V2ZW50TGlzdGVuZXJCb3VuZCk7XHJcbiAgICB0aGlzLl9jb250cm9scy5yZWdpc3Rlckxpc3RlbmVyKE1PVkUuQk9UVE9NLCB0aGlzLl9jb250cm9sc0V2ZW50TGlzdGVuZXJCb3VuZCk7XHJcbiAgICB0aGlzLl9jb250cm9scy5yZWdpc3Rlckxpc3RlbmVyKE1PVkUuTEVGVCwgdGhpcy5fY29udHJvbHNFdmVudExpc3RlbmVyQm91bmQpO1xyXG4gICAgdGhpcy5fY29udHJvbHMucmVnaXN0ZXJMaXN0ZW5lcihNT1ZFLlJJR0hULCB0aGlzLl9jb250cm9sc0V2ZW50TGlzdGVuZXJCb3VuZCk7XHJcbiAgICB0aGlzLl9jb250cm9scy5yZWdpc3Rlckxpc3RlbmVyKE1PVkUuUkVTRVQsIHRoaXMuX2NvbnRyb2xzRXZlbnRMaXN0ZW5lckJvdW5kKTtcclxuXHJcbiAgICAvLyBpbml0IGdyaWRcclxuICAgIHRoaXMuX2dyaWQuaW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfY29udHJvbHNFdmVudGxpc3RlbmVyKG1vdmU6IG51bWJlcikge1xyXG4gICAgaWYgKG1vdmUgPT0gTU9WRS5SRVNFVCl7XHJcbiAgICAgIHRoaXMuX2dyaWQucmVzZXQoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2dyaWQubW92ZShtb3ZlKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxufVxyXG5cclxuY29uc3QgbWFpbiA9IG5ldyBNYWluKCk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vU3JjL1NjcmlwdHMvTWFpbi50cyIsImltcG9ydCB7IE1PVkUgfSBmcm9tICcuL1RpbGUnO1xyXG5cclxuaW50ZXJmYWNlIElMaXN0ZW5lciB7XHJcbiAgKG1vdmU6IG51bWJlcik6IHZvaWRcclxufVxyXG5cclxuaW50ZXJmYWNlIElMaXN0ZW5lcnMge1xyXG4gIFtrZXk6IG51bWJlcl06IElMaXN0ZW5lcltdXHJcbn1cclxuXHJcbmVudW0gS0VZIHtcclxuICBMRUZUID0gMzcsXHJcbiAgVVAgPSAzOCxcclxuICBSSUdIVCA9IDM5LFxyXG4gIERPV04gPSA0MCxcclxuICBSID0gODJcclxufVxyXG5cclxuY2xhc3MgQ29udHJvbHMge1xyXG4gIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IEtFWV9FVkVOVCA9ICdrZXlkb3duJztcclxuICBwcml2YXRlIHN0YXRpYyByZWFkb25seSBUT1VDSF9FVkVOVCA9IHtcclxuICAgIFNUQVJUOiAndG91Y2hzdGFydCcsXHJcbiAgICBFTkQ6ICd0b3VjaGVuZCcsXHJcbiAgICBNT1ZFOiAndG91Y2htb3ZlJyxcclxuXHJcbiAgICBNUzoge1xyXG4gICAgICBTVEFSVDogJ01TUG9pbmVyRG93bicsXHJcbiAgICAgIEVORDogJ01TUG9pbnRlck1vdmUnLFxyXG4gICAgICBNT1ZFOiAnTVNQb2ludGVyVXAnLFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfa2V5Ym9hcmRFdmVudEJvdW5kO1xyXG4gIHByaXZhdGUgX2xpc3RlbmVyczogSUxpc3RlbmVycyA9IHt9O1xyXG5cclxuICBwcml2YXRlIF9lbGVtZW50TmV3R2FtZTogSFRNTEVsZW1lbnQ7IFxyXG5cclxuICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLl9pbml0S2V5Ym9hcmRDb250cm9scygpO1xyXG4gICAgdGhpcy5faW5pdEdlc3R1cmVDb250cm9scygpO1xyXG4gICAgdGhpcy5faW5pdFZpZXdDb250cm9scygpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHJlZ2lzdGVyTGlzdGVuZXIobW92ZTogbnVtYmVyLCBsaXN0ZW5lcikge1xyXG4gICAgaWYgKCF0aGlzLl9saXN0ZW5lcnNbbW92ZV0pIHRoaXMuX2xpc3RlbmVyc1ttb3ZlXSA9IFtdO1xyXG4gICAgdGhpcy5fbGlzdGVuZXJzW21vdmVdLnB1c2gobGlzdGVuZXIpO1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfaW5pdFZpZXdDb250cm9scygpIHtcclxuICAgIHRoaXMuX2VsZW1lbnROZXdHYW1lID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2J0bi1uZXctZ2FtZScpO1xyXG4gICAgdGhpcy5fZWxlbWVudE5ld0dhbWUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLl9jYWxsTGlzdGVuZXJzLmJpbmQodGhpcywgTU9WRS5SRVNFVCkpO1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfaW5pdEtleWJvYXJkQ29udHJvbHMoKSB7XHJcbiAgICB0aGlzLl9rZXlib2FyZEV2ZW50Qm91bmQgPSB0aGlzLl9rZXlib2FyZEV2ZW50LmJpbmQodGhpcyk7XHJcblxyXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihDb250cm9scy5LRVlfRVZFTlQsIHRoaXMuX2tleWJvYXJkRXZlbnRCb3VuZCk7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9pbml0R2VzdHVyZUNvbnRyb2xzKCkge1xyXG4gICAgY29uc3QgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdncmlkJyk7XHJcblxyXG4gICAgaWYgKCFlbGVtZW50KSByZXR1cm47XHJcblxyXG4gICAgY29uc3QgdG91Y2hFdmVudHMgPSB3aW5kb3cubmF2aWdhdG9yLm1zUG9pbnRlckVuYWJsZWRcclxuICAgICAgPyBDb250cm9scy5UT1VDSF9FVkVOVC5NU1xyXG4gICAgICA6IENvbnRyb2xzLlRPVUNIX0VWRU5UXHJcblxyXG4gICAgbGV0IHN0YXJ0WCA9IDA7XHJcbiAgICBsZXQgc3RhcnRZID0gMDtcclxuXHJcbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIodG91Y2hFdmVudHMuU1RBUlQsIChldmVudDogVG91Y2hFdmVudCkgPT4ge1xyXG4gICAgICBpZiAod2luZG93Lm5hdmlnYXRvci5tc1BvaW50ZXJFbmFibGVkKSB7XHJcbiAgICAgICAgc3RhcnRYID0gKGV2ZW50IGFzIGFueSkucGFnZVg7XHJcbiAgICAgICAgc3RhcnRZID0gKGV2ZW50IGFzIGFueSkucGFnZVk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc3RhcnRYID0gZXZlbnQudG91Y2hlc1swXS5jbGllbnRYO1xyXG4gICAgICAgIHN0YXJ0WSA9IGV2ZW50LnRvdWNoZXNbMF0uY2xpZW50WTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKHRvdWNoRXZlbnRzLk1PVkUsIChldmVudCkgPT4ge1xyXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9KTtcclxuXHJcbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIodG91Y2hFdmVudHMuRU5ELCAoZXZlbnQ6IFRvdWNoRXZlbnQpID0+IHtcclxuICAgICAgbGV0IGVuZFggPSAwO1xyXG4gICAgICBsZXQgZW5kWSA9IDA7XHJcblxyXG4gICAgICBpZiAod2luZG93Lm5hdmlnYXRvci5tc1BvaW50ZXJFbmFibGVkKSB7XHJcbiAgICAgICAgZW5kWCA9IChldmVudCBhcyBhbnkpLnBhZ2VYO1xyXG4gICAgICAgIGVuZFkgPSAoZXZlbnQgYXMgYW55KS5wYWdlWTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBlbmRYID0gZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0uY2xpZW50WDtcclxuICAgICAgICBlbmRZID0gZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0uY2xpZW50WTtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgZGlmZlggPSBzdGFydFggLSBlbmRYO1xyXG4gICAgICBjb25zdCBkaWZmWSA9IHN0YXJ0WSAtIGVuZFk7XHJcblxyXG4gICAgICBjb25zdCBhYnNEaWZmWCA9IE1hdGguYWJzKGRpZmZYKTtcclxuICAgICAgY29uc3QgYWJzRGlmZlkgPSBNYXRoLmFicyhkaWZmWSk7XHJcblxyXG4gICAgICBpZiAoTWF0aC5tYXgoYWJzRGlmZlgsIGFic0RpZmZZKSA+IDEwKSB7XHJcbiAgICAgICAgdGhpcy5fY2FsbExpc3RlbmVycyhhYnNEaWZmWCA+IGFic0RpZmZZXHJcbiAgICAgICAgICA/IGRpZmZYID4gMCA/IE1PVkUuTEVGVCA6IE1PVkUuUklHSFRcclxuICAgICAgICAgIDogZGlmZlkgPiAwID8gTU9WRS5UT1AgOiBNT1ZFLkJPVFRPTVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2tleWJvYXJkRXZlbnQoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgIGNvbnN0IGhhc01vZGlmaWVyID0gZXZlbnQuYWx0S2V5IHx8IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQubWV0YUtleSB8fCBldmVudC5zaGlmdEtleVxyXG5cclxuICAgIC8vIGlmIG1vZGlmaWVyIGlzIHByZXNzZWQsIGV4aXRcclxuICAgIGlmIChoYXNNb2RpZmllcikgXHJcbiAgICAgIHJldHVybjtcclxuXHJcbiAgICBzd2l0Y2ggKGV2ZW50LndoaWNoKSB7XHJcbiAgICAgIGNhc2UgS0VZLkRPV046XHJcbiAgICAgICAgdGhpcy5fY2FsbExpc3RlbmVycyhNT1ZFLkJPVFRPTSk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgS0VZLlVQOlxyXG4gICAgICAgIHRoaXMuX2NhbGxMaXN0ZW5lcnMoTU9WRS5UT1ApO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIEtFWS5MRUZUOlxyXG4gICAgICAgIHRoaXMuX2NhbGxMaXN0ZW5lcnMoTU9WRS5MRUZUKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBLRVkuUklHSFQ6XHJcbiAgICAgICAgdGhpcy5fY2FsbExpc3RlbmVycyhNT1ZFLlJJR0hUKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBLRVkuUjpcclxuICAgICAgICB0aGlzLl9jYWxsTGlzdGVuZXJzKE1PVkUuUkVTRVQpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2NhbGxMaXN0ZW5lcnMobW92ZTogbnVtYmVyKSB7XHJcbiAgICBpZiAodGhpcy5fbGlzdGVuZXJzW21vdmVdKSB7XHJcbiAgICAgIHRoaXMuX2xpc3RlbmVyc1ttb3ZlXS5mb3JFYWNoKChsaXN0ZW5lcikgPT4ge1xyXG4gICAgICAgIGxpc3RlbmVyKG1vdmUpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQ29udHJvbHM7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vU3JjL1NjcmlwdHMvQ29udHJvbHMudHMiLCJpbXBvcnQgU2xvdCBmcm9tICcuL1Nsb3QnO1xyXG5pbXBvcnQgVGlsZSwgeyBNT1ZFLCBJTG9jYXRpb24yZCB9IGZyb20gJy4vVGlsZSc7XHJcbmltcG9ydCBTY29yZSBmcm9tICcuL1Njb3JlJztcclxuXHJcbmludGVyZmFjZSBJTW92ZVJlc3VsdEl0ZXJhdGVlIHtcclxuICAoc2xvdDogU2xvdCwgdGFyZ2V0U2xvdDogU2xvdCk6IHZvaWRcclxufVxyXG5cclxuZW51bSBQTEFZX1NUQVRFIHtcclxuICBPTkdPSUdOLFxyXG4gIFdJTixcclxuICBMT1NUXHJcbn1cclxuXHJcbmNsYXNzIEdyaWQge1xyXG4gIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IF9XSU5fVkFMVUUgPSAyMDQ4O1xyXG5cclxuICBwdWJsaWMgc3RhdGljIEVsR3JpZDogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjZ3JpZCcpIGFzIEhUTUxFbGVtZW50O1xyXG5cclxuICBwcml2YXRlIF9pc0xvY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHByaXZhdGUgX2hhc1VwZGF0ZWRPbkxhc3RNb3ZlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSBfZ3JpZFNpemU6IG51bWJlciA9IDQ7XHJcbiAgcHJpdmF0ZSBfbWF0cml4OiBTbG90W10gPSBbXTtcclxuICBwcml2YXRlIF9pZHhTbG90c0VtcHR5OiBTZXQ8bnVtYmVyPiA9IG5ldyBTZXQoKTtcclxuICBwcml2YXRlIF9pZHhTbG90c09jY3VwaWVkOiBTZXQ8bnVtYmVyPiA9IG5ldyBTZXQoKTtcclxuICBwcml2YXRlIF9zY29yZTogU2NvcmU7XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIF9Ub0FycmF5PFQ+KGl0ZXJhdG9yOiBJdGVyYXRvcjxUPikge1xyXG4gICAgY29uc3QgYXJyID0gW107XHJcbiAgICBsZXQgbmV4dDogSXRlcmF0b3JSZXN1bHQ8VD4gPSBpdGVyYXRvci5uZXh0KCk7XHJcbiAgICBcclxuICAgIGRvIHtcclxuICAgICAgaWYgKHR5cGVvZiBuZXh0LnZhbHVlICE9ICd1bmRlZmluZWQnKSBhcnIucHVzaChuZXh0LnZhbHVlKTtcclxuICAgICAgbmV4dCA9IGl0ZXJhdG9yLm5leHQoKTtcclxuICAgIH0gd2hpbGUgKCFuZXh0LmRvbmUpO1xyXG5cclxuICAgIHJldHVybiBhcnI7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY29uc3RydWN0b3IoZ3JpZFNpemU6IG51bWJlcikge1xyXG4gICAgdGhpcy5fZ3JpZFNpemUgPSBncmlkU2l6ZTtcclxuXHJcbiAgICBTbG90LkVsR3JpZCA9IHRoaXMuY29uc3RydWN0b3IuRWxHcmlkO1xyXG4gICAgU2xvdC5HcmlkU2l6ZSA9IHRoaXMuX2dyaWRTaXplO1xyXG5cclxuICAgIHRoaXMuX3Njb3JlID0gbmV3IFNjb3JlKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgaW5pdCgpIHtcclxuICAgIHRoaXMuX21hdHJpeC5sZW5ndGggPSB0aGlzLl9ncmlkU2l6ZSAqIHRoaXMuX2dyaWRTaXplO1xyXG5cclxuICAgIC8vIGluaXRpYWxpemUgc2xvdHNcclxuICAgIHRoaXMuX21hdHJpeCA9IHRoaXMuX21hdHJpeFxyXG4gICAgICAuZmlsbCh1bmRlZmluZWQpXHJcbiAgICAgIC5tYXAoKHZhbHVlLCBpZHgpID0+IHtcclxuICAgICAgICB0aGlzLl9pZHhTbG90c0VtcHR5LmFkZChpZHgpO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFNsb3QoaWR4KTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgdGhpcy5nZW5lcmF0ZVRpbGUoKTtcclxuICAgIHRoaXMuZ2VuZXJhdGVUaWxlKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYXN5bmMgbW92ZShtb3ZlOiBudW1iZXIpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIGlmICh0aGlzLl9pc0xvY2tlZCkgcmV0dXJuO1xyXG5cclxuICAgIGNvbnN0IGNvbmN1cnJlbnRFeGVjdXRpb24gPSBbXTtcclxuICAgIFxyXG4gICAgdGhpcy5faGFzVXBkYXRlZE9uTGFzdE1vdmUgPSBmYWxzZTtcclxuICAgIFxyXG4gICAgLy8gYXBwbHkgbW92ZSB0byBlYWNoIHZlY3RvclxyXG4gICAgZm9yIChsZXQgaWR4ID0gMCwgbCA9IHRoaXMuX21hdHJpeC5sZW5ndGg7IGlkeCA8IGw7IGlkeCArPSB0aGlzLl9ncmlkU2l6ZSArIDEpIHtcclxuICAgICAgLy8gZXhlY3V0ZSBjb25jdXJyZWx0eVxyXG4gICAgICBjb25jdXJyZW50RXhlY3V0aW9uLnB1c2godGhpcy5tb3ZlVmVjdG9yLmNhbGwodGhpcywgaWR4LCBtb3ZlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gd2FpdCB1bnRpbGwgcmVuZGVyaW5nIGNvbXBsZXRlc1xyXG4gICAgYXdhaXQgUHJvbWlzZS5hbGwoY29uY3VycmVudEV4ZWN1dGlvbik7XHJcblxyXG4gICAgdGhpcy5fdXBkYXRlUGxheVN0YXRlKCk7XHJcblxyXG4gICAgLy8gZ2VuZXJhdGUgbmV3IHRpbGUgaWYgbWF0cml4IHdhcyB1cGRhdGVkXHJcbiAgICBpZiAodGhpcy5faGFzVXBkYXRlZE9uTGFzdE1vdmUpIHRoaXMuZ2VuZXJhdGVUaWxlKCk7XHJcbiAgICBcclxuICAgIHRoaXMuX1ByaW50KCk7XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHJlc2V0KCkge1xyXG5cclxuICAgIC8vIHJlc2V0IHNjb3JlXHJcbiAgICB0aGlzLl9zY29yZS5yZXNldCgpO1xyXG4gICAgdGhpcy5fc2NvcmUudmlld1JlbmRlcigpO1xyXG5cclxuICAgIC8vIHJlc2V0IG1hdHJpeFxyXG4gICAgdGhpcy5fbWF0cml4XHJcbiAgICAgIC5mb3JFYWNoKChzbG90LCBpZHgpID0+IHtcclxuICAgICAgICB0aGlzLl9pZHhTbG90c0VtcHR5LmFkZChpZHgpO1xyXG4gICAgICAgIHRoaXMuX2lkeFNsb3RzT2NjdXBpZWQuZGVsZXRlKGlkeCk7XHJcblxyXG4gICAgICAgIHNsb3QuZ2V0VGlsZSgpLmRlc3RydWN0b3IoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gcmVzZXQgc3RhdGVcclxuICAgIHRoaXMuX3VwZGF0ZVBsYXlTdGF0ZSgpO1xyXG5cclxuICAgIC8vIHBsYWNlIHRpbGVzXHJcbiAgICB0aGlzLmdlbmVyYXRlVGlsZSgpO1xyXG4gICAgdGhpcy5nZW5lcmF0ZVRpbGUoKTtcclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgYXN5bmMgbW92ZVZlY3RvcihpZHg6IG51bWJlciwgbW92ZTogbnVtYmVyKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICBjb25zdCB2ZWN0b3JJbmRpY2VzID0gdGhpcy5nZXRNb3ZlVmVjdG9ySW5kaWNlcyhpZHgsIG1vdmUpO1xyXG4gICAgY29uc3QgdmVjdG9yID0gdGhpcy5nZXRNb3ZlVmVjdG9yKGlkeCwgbW92ZSk7XHJcblxyXG4gICAgbGV0IGhhc0l0ZXJhdGVlRmlyZWQgPSBmYWxzZTtcclxuXHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8dm9pZD4oKGZ1bGZpbGwpID0+IHtcclxuICAgICAgdGhpcy5jYWxjTW92ZVJlc3VsdCh2ZWN0b3IsIGFzeW5jIChzbG90LCB0YXJnZXRTbG90KSA9PiB7XHJcbiAgICAgICAgaGFzSXRlcmF0ZWVGaXJlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5faGFzVXBkYXRlZE9uTGFzdE1vdmUgPSB0cnVlO1xyXG4gICAgICAgIGF3YWl0IHRoaXMubW92ZUl0ZXJhdGVlKHNsb3QsIHRhcmdldFNsb3QsIG1vdmUpO1xyXG4gICAgICAgIHJldHVybiBmdWxmaWxsKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKCFoYXNJdGVyYXRlZUZpcmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bGZpbGwoKTtcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBhc3luYyBtb3ZlSXRlcmF0ZWUoXHJcbiAgICBzbG90OiBTbG90LCBcclxuICAgIHRhcmdldFNsb3Q6IFNsb3QsIFxyXG4gICAgbW92ZTogbnVtYmVyXHJcbiAgKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICAvLyBpZiBzb3VyY2UgaXMgdGhlIHNhbWUgYXMgdGFyZ2V0LCBza2lwIGV4ZWN1dGlvblxyXG4gICAgaWYgKHNsb3QgPT0gdGFyZ2V0U2xvdClcclxuICAgICAgcmV0dXJuO1xyXG5cclxuICAgIGNvbnN0IHRpbGUgPSBzbG90LmdldFRpbGUoKTtcclxuICAgIGNvbnN0IHRhcmdldFRpbGUgPSB0YXJnZXRTbG90LmdldFRpbGUoKTtcclxuXHJcbiAgICAvLyB1cGRhdGUgaW5kaWNlc1xyXG4gICAgdGhpcy5faWR4U2xvdHNPY2N1cGllZC5hZGQodGFyZ2V0U2xvdC5nZXRJZHgoKSk7XHJcbiAgICB0aGlzLl9pZHhTbG90c09jY3VwaWVkLmRlbGV0ZShzbG90LmdldElkeCgpKTtcclxuICAgIHRoaXMuX2lkeFNsb3RzRW1wdHkuYWRkKHNsb3QuZ2V0SWR4KCkpO1xyXG4gICAgdGhpcy5faWR4U2xvdHNFbXB0eS5kZWxldGUodGFyZ2V0U2xvdC5nZXRJZHgoKSk7XHJcblxyXG4gICAgLy8gcmVuZGVyIHJlbGF0ZWQgcHJvY2Vzc2luZ1xyXG4gICAgY29uc3QgbG9jYXRpb24yZCA9IHRoaXMudmlld0NhbGNMb2NhdGlvbjJkKHNsb3QsIHRhcmdldFNsb3QsIG1vdmUpO1xyXG4gICAgdGlsZS5zZXRMb2NhdGlvbjJkKGxvY2F0aW9uMmQueCwgbG9jYXRpb24yZC55KTtcclxuXHJcbiAgICBhd2FpdCB0aWxlLnZpZXdSZW5kZXJNb3ZlKCk7XHJcbiAgICBpZiAodGlsZS5nZXRWYWx1ZSgpKSB7XHJcbiAgICAgIHRpbGUucmVuZGVyUmVzZXQoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRpbGUuZGVzdHJ1Y3RvcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHRhcmdldFRpbGUucmVuZGVyKCk7XHJcblxyXG4gICAgaWYgKHRhcmdldFRpbGUuZ2V0SGFzVmFsdWVEb3VibGVkKCkpIHtcclxuICAgICAgdGFyZ2V0VGlsZS5yZW5kZXJQdWxzZSgpO1xyXG5cclxuICAgICAgdGhpcy5fc2NvcmUuYWRkKHRhcmdldFRpbGUuZ2V0VmFsdWUoKSk7XHJcbiAgICAgIHRoaXMuX3Njb3JlLnZpZXdSZW5kZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgZ2VuZXJhdGVUaWxlKCkge1xyXG4gICAgaWYgKHRoaXMuX2lzTG9ja2VkKSByZXR1cm47XHJcblxyXG4gICAgY29uc3Qgc2l6ZSA9IHRoaXMuX2lkeFNsb3RzRW1wdHkuc2l6ZTtcclxuXHJcbiAgICBpZiAoIXNpemUpIHJldHVybjtcclxuXHJcbiAgICBjb25zdCBzbG90cyA9IEdyaWQuX1RvQXJyYXkodGhpcy5faWR4U2xvdHNFbXB0eS52YWx1ZXMoKSk7XHJcbiAgICBjb25zdCByYW5kb21JZHggPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoc2l6ZSAtIDEpKTtcclxuICAgIGNvbnN0IHNsb3RJZHggPSBzbG90c1tyYW5kb21JZHhdO1xyXG5cclxuICAgIC8vIHBsYWNlIHRpbGUgb24gbWF0cml4XHJcbiAgICB0aGlzLl9tYXRyaXhbc2xvdElkeF0uZ2V0VGlsZSgpLmdlbmVyYXRlVmFsdWUoKTtcclxuICAgIHRoaXMuX21hdHJpeFtzbG90SWR4XS5nZXRUaWxlKCkucmVuZGVyKCk7XHJcblxyXG4gICAgLy8gdXBkYXRlIGluZGV4ZXNcclxuICAgIHRoaXMuX2lkeFNsb3RzRW1wdHkuZGVsZXRlKHNsb3RJZHgpO1xyXG4gICAgdGhpcy5faWR4U2xvdHNPY2N1cGllZC5hZGQoc2xvdElkeCk7XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNhbGNNb3ZlUmVzdWx0KFxyXG4gICAgdmVjdG9yOiBTbG90W10sIFxyXG4gICAgaXRlcmF0ZWU6IElNb3ZlUmVzdWx0SXRlcmF0ZWUgPSB1bmRlZmluZWRcclxuICApOiBTbG90W10ge1xyXG4gICAgZm9yIChsZXQgaSA9IDAsIGwgPSB0aGlzLl9ncmlkU2l6ZSAtIDE7IGkgPCBsOyBpKyspIHtcclxuICAgICAgY29uc3Qgc2xvdCA9IHZlY3RvcltpXTtcclxuXHJcbiAgICAgIGxldCBqID0gaSArIDE7XHJcbiAgICAgIGxldCBpc1BhaXJGb3VuZCA9IGZhbHNlO1xyXG5cclxuICAgICAgd2hpbGUoIWlzUGFpckZvdW5kICYmIGogPCB0aGlzLl9ncmlkU2l6ZSkge1xyXG4gICAgICAgIGNvbnN0IG5leHRTbG90ID0gdmVjdG9yW2pdO1xyXG5cclxuICAgICAgICAvLyB0aWxlIGlzIG5vdCBlbXBseVxyXG4gICAgICAgIGlmIChzbG90LmdldFRpbGUoKS5nZXRWYWx1ZSgpKSB7XHJcbiAgICAgICAgICBpZiAobmV4dFNsb3QuZ2V0VGlsZSgpLmdldFZhbHVlKCkgPT0gc2xvdC5nZXRUaWxlKCkuZ2V0VmFsdWUoKSkge1xyXG4gICAgICAgICAgICBzbG90LmdldFRpbGUoKS5kb3VibGVWYWx1ZSgpO1xyXG4gICAgICAgICAgICBuZXh0U2xvdC5nZXRUaWxlKCkuY2xlYXJWYWx1ZSgpO1xyXG4gICAgICAgICAgICBpc1BhaXJGb3VuZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICBpZiAoaXRlcmF0ZWUpIGl0ZXJhdGVlKG5leHRTbG90LCBzbG90KTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAobmV4dFNsb3QuZ2V0VGlsZSgpLmdldFZhbHVlKCkpIHtcclxuICAgICAgICAgICAgaXNQYWlyRm91bmQgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAobmV4dFNsb3QuZ2V0VGlsZSgpLmdldFZhbHVlKCkpIHtcclxuICAgICAgICAgIHNsb3QuZ2V0VGlsZSgpLnNldFZhbHVlKG5leHRTbG90LmdldFRpbGUoKS5nZXRWYWx1ZSgpKTtcclxuICAgICAgICAgIG5leHRTbG90LmdldFRpbGUoKS5jbGVhclZhbHVlKCk7XHJcblxyXG4gICAgICAgICAgaWYgKGl0ZXJhdGVlKSBpdGVyYXRlZShuZXh0U2xvdCwgc2xvdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBqKys7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdmVjdG9yO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGdldE1vdmVWZWN0b3IoaWR4OiBudW1iZXIsIG1vdmU6IG51bWJlcik6IFNsb3RbXSB7XHJcbiAgICBjb25zdCB2ZWN0b3JJbmRpY2VzID0gdGhpcy5nZXRNb3ZlVmVjdG9ySW5kaWNlcyhpZHgsIG1vdmUpO1xyXG4gICAgY29uc3QgdmVjdG9yID0gdmVjdG9ySW5kaWNlcy5tYXAoaWR4ID0+IHRoaXMuX21hdHJpeFtpZHhdKTtcclxuXHJcbiAgICByZXR1cm4gdmVjdG9yO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGdldE1vdmVWZWN0b3JJbmRpY2VzKGlkeDogbnVtYmVyLCBtb3ZlOiBudW1iZXIpOiBudW1iZXJbXSB7XHJcbiAgICBjb25zdCBsZW5ndGggPSB0aGlzLl9tYXRyaXgubGVuZ3RoO1xyXG4gICAgY29uc3QgY29sdW1uID0gaWR4ICUgdGhpcy5fZ3JpZFNpemU7XHJcbiAgICBjb25zdCByb3cgPSBNYXRoLmZsb29yKGlkeCAvIHRoaXMuX2dyaWRTaXplKTtcclxuXHJcbiAgICBjb25zdCB2ZWN0b3I6IG51bWJlcltdID0gW107XHJcblxyXG4gICAgLy8gZ2V0IGluaXRpYWwgaW5kZXhcclxuICAgIHN3aXRjaCAobW92ZSkge1xyXG4gICAgICBjYXNlIChNT1ZFLlRPUCk6IFxyXG4gICAgICAgIGlkeCA9IGNvbHVtbjsgXHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgKE1PVkUuQk9UVE9NKTogXHJcbiAgICAgICAgaWR4ID0gbGVuZ3RoIC0gKHRoaXMuX2dyaWRTaXplIC0gY29sdW1uKTsgXHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgKE1PVkUuTEVGVCk6IFxyXG4gICAgICAgIGlkeCA9IHJvdyAqIHRoaXMuX2dyaWRTaXplOyBcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAoTU9WRS5SSUdIVCk6IFxyXG4gICAgICAgIGlkeCA9IHJvdyAqIHRoaXMuX2dyaWRTaXplICsgKHRoaXMuX2dyaWRTaXplIC0gMSk7IFxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5fZ3JpZFNpemU7IGkrKykge1xyXG4gICAgICB2ZWN0b3IucHVzaChpZHgpO1xyXG5cclxuICAgICAgLy8gY2FsYyBuZXh0IGluZGV4XHJcbiAgICAgIHN3aXRjaCAobW92ZSkge1xyXG4gICAgICAgIGNhc2UgKE1PVkUuVE9QKTogXHJcbiAgICAgICAgICBpZHggKz0gdGhpcy5fZ3JpZFNpemU7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlIChNT1ZFLkJPVFRPTSk6IFxyXG4gICAgICAgICAgaWR4IC09IHRoaXMuX2dyaWRTaXplO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAoTU9WRS5MRUZUKTogXHJcbiAgICAgICAgICBpZHgrKztcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgKE1PVkUuUklHSFQpOiBcclxuICAgICAgICAgIGlkeC0tO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdmVjdG9yO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIHZpZXdDYWxjTG9jYXRpb24yZChzbG90OiBTbG90LCB0YXJnZXRTbG90OiBTbG90LCBtb3ZlOiBudW1iZXIpOiBJTG9jYXRpb24yZCB7XHJcbiAgICBjb25zdCB2ZWN0b3JJbmRpY2VzID0gdGhpcy5nZXRNb3ZlVmVjdG9ySW5kaWNlcyhzbG90LmdldElkeCgpLCBtb3ZlKTtcclxuICAgIGNvbnN0IGlkeERpZmYgPSBNYXRoLmFicyh2ZWN0b3JJbmRpY2VzLmluZGV4T2Yoc2xvdC5nZXRJZHgoKSkgLSB2ZWN0b3JJbmRpY2VzLmluZGV4T2YodGFyZ2V0U2xvdC5nZXRJZHgoKSkpO1xyXG4gICAgY29uc3QgY2xpZW50U2xvdFNpemUgPSBzbG90LnZpZXdHZXRDbGllbnRXaWR0aCgpO1xyXG4gICAgY29uc3QgbG9jYXRpb24yZDogSUxvY2F0aW9uMmQgPSB7XHJcbiAgICAgIHg6IDAsXHJcbiAgICAgIHk6IDBcclxuICAgIH1cclxuXHJcbiAgICBzd2l0Y2ggKG1vdmUpIHtcclxuICAgICAgY2FzZSBNT1ZFLlRPUDpcclxuICAgICAgICBsb2NhdGlvbjJkLnkgPSBpZHhEaWZmICogY2xpZW50U2xvdFNpemUgKiAtMTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBNT1ZFLkJPVFRPTTpcclxuICAgICAgICBsb2NhdGlvbjJkLnkgPSBpZHhEaWZmICogY2xpZW50U2xvdFNpemU7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgTU9WRS5MRUZUOlxyXG4gICAgICAgIGxvY2F0aW9uMmQueCA9IGlkeERpZmYgKiBjbGllbnRTbG90U2l6ZSAqIC0xO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIE1PVkUuUklHSFQ6XHJcbiAgICAgICAgbG9jYXRpb24yZC54ID0gaWR4RGlmZiAqIGNsaWVudFNsb3RTaXplO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBsb2NhdGlvbjJkO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfdmlld1JlbmRlclBsYXlTdGF0ZShzdGF0ZTogbnVtYmVyKSB7XHJcbiAgICBzd2l0Y2ggKHN0YXRlKSB7XHJcbiAgICAgIGNhc2UgUExBWV9TVEFURS5PTkdPSUdOOlxyXG4gICAgICAgIHRoaXMuX2lzTG9ja2VkID0gZmFsc2U7XHJcbiAgICAgICAgR3JpZC5FbEdyaWQuY2xhc3NMaXN0LnJlbW92ZSgnaGFzLXdvbicpO1xyXG4gICAgICAgIEdyaWQuRWxHcmlkLmNsYXNzTGlzdC5yZW1vdmUoJ2hhcy1sb3N0Jyk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgUExBWV9TVEFURS5XSU46XHJcbiAgICAgICAgdGhpcy5faXNMb2NrZWQgPSB0cnVlO1xyXG4gICAgICAgIEdyaWQuRWxHcmlkLmNsYXNzTGlzdC5yZW1vdmUoJ2hhcy1sb3N0Jyk7XHJcbiAgICAgICAgR3JpZC5FbEdyaWQuY2xhc3NMaXN0LmFkZCgnaGFzLXdvbicpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIFBMQVlfU1RBVEUuTE9TVDpcclxuICAgICAgICB0aGlzLl9pc0xvY2tlZCA9IHRydWU7XHJcbiAgICAgICAgR3JpZC5FbEdyaWQuY2xhc3NMaXN0LnJlbW92ZSgnaGFzLXdvbicpO1xyXG4gICAgICAgIEdyaWQuRWxHcmlkLmNsYXNzTGlzdC5hZGQoJ2hhcy1sb3N0Jyk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfdXBkYXRlUGxheVN0YXRlKCkge1xyXG4gICAgY29uc3Qgc2xvdE9jY3VwaWVkSW5kaWNlcyA9IEdyaWQuX1RvQXJyYXkodGhpcy5faWR4U2xvdHNPY2N1cGllZC52YWx1ZXMoKSk7XHJcbiAgICBcclxuICAgIGxldCBoYXNXb24gPSBmYWxzZTtcclxuICAgIGxldCBpID0gMDtcclxuXHJcbiAgICB3aGlsZSAoIWhhc1dvbiAmJiBpIDwgc2xvdE9jY3VwaWVkSW5kaWNlcy5sZW5ndGgpIHtcclxuICAgICAgY29uc3Qgc2xvdCA9IHRoaXMuX21hdHJpeFtzbG90T2NjdXBpZWRJbmRpY2VzW2ldXTtcclxuICAgICAgaGFzV29uID0gc2xvdC5nZXRUaWxlKCkuZ2V0VmFsdWUoKSA9PSBHcmlkLl9XSU5fVkFMVUU7XHJcbiAgICAgIGkrKztcclxuICAgIH1cclxuICAgIFxyXG4gICAgaWYgKGhhc1dvbikge1xyXG4gICAgICB0aGlzLl9pc0xvY2tlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuX3ZpZXdSZW5kZXJQbGF5U3RhdGUoUExBWV9TVEFURS5XSU4pO1xyXG4gICAgfSBlbHNlIGlmICghdGhpcy5faWR4U2xvdHNFbXB0eS5zaXplKSB7XHJcbiAgICAgIHRoaXMuX2lzTG9ja2VkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5fdmlld1JlbmRlclBsYXlTdGF0ZShQTEFZX1NUQVRFLkxPU1QpO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLl9pc0xvY2tlZCkge1xyXG4gICAgICB0aGlzLl9pc0xvY2tlZCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl92aWV3UmVuZGVyUGxheVN0YXRlKFBMQVlfU1RBVEUuT05HT0lHTik7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfUHJpbnQoKSB7XHJcbiAgICBjb25zdCBmb3JtYXR0ZWRWYWx1ZXMgPSBbXTtcclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuX21hdHJpeC5sZW5ndGg7IGkrKykge1xyXG4gICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuX21hdHJpeFtpXS5nZXRUaWxlKCkuZ2V0VmFsdWUoKS50b1N0cmluZygpO1xyXG4gICAgICBmb3JtYXR0ZWRWYWx1ZXMucHVzaCgnICAgICAnLnN1YnN0cmluZygwLCA1IC0gdmFsdWUubGVuZ3RoKSArIHZhbHVlKTtcclxuXHJcbiAgICAgIGlmICgoaSArIDEpICUgdGhpcy5fZ3JpZFNpemUgPT0gMCkge1xyXG4gICAgICAgIGZvcm1hdHRlZFZhbHVlcy5wdXNoKCdcXG4nKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb25zb2xlLmxvZyhmb3JtYXR0ZWRWYWx1ZXMuam9pbignJykpO1xyXG4gIH1cclxuXHJcbiAgLy8gTk9URTogdHMgMi41IGRvZXMgbm90IGhhdmUgc3Ryb25nbHkgdHlwZWQgY29uc3RydWN0b3JzIGFuZFxyXG4gIC8vIHRoaXMgd29ya2Fyb3VuZCBhbGxvd3MgdG8gY2FsbCBzdGF0aWMgcHJvcGVydGllcyBvZiBhIGNsYXNzLlxyXG4gIC8vIFJlZmVyZW5jZTogaHR0cHM6Ly9naXRodWIuY29tL01pY3Jvc29mdC9UeXBlU2NyaXB0L2lzc3Vlcy8zODQxI2lzc3VlY29tbWVudC0yOTgxOTk4MzVcclxuICAnY29uc3RydWN0b3InOiB0eXBlb2YgR3JpZDtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgR3JpZDtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9TcmMvU2NyaXB0cy9HcmlkLnRzIiwiaW1wb3J0IFRpbGUgZnJvbSAnLi9UaWxlJztcclxuXHJcbmNsYXNzIFNsb3Qge1xyXG4gIHB1YmxpYyBzdGF0aWMgR3JpZFNpemUgPSAwO1xyXG4gIHB1YmxpYyBzdGF0aWMgRWxHcmlkOiBIVE1MRWxlbWVudCA9IHVuZGVmaW5lZDtcclxuXHJcbiAgcHJpdmF0ZSBfdGlsZTogVGlsZSA9IHVuZGVmaW5lZDtcclxuICBwcml2YXRlIF9pZHg6IG51bWJlciA9IC0xO1xyXG4gIHByaXZhdGUgX2VsZW1lbnQ6IEhUTUxFbGVtZW50ID0gdW5kZWZpbmVkO1xyXG5cclxuICBwdWJsaWMgY29uc3RydWN0b3IoaWR4OiBudW1iZXIsIHRpbGU6IFRpbGUgPSB1bmRlZmluZWQpIHtcclxuICAgIHRoaXMuX2lkeCA9IGlkeDtcclxuICAgIHRoaXMuX3RpbGUgPSB0aWxlIHx8IG5ldyBUaWxlKCk7XHJcbiAgICB0aGlzLl9jcmVhdGVFbGVtZW50KCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0SWR4KCkgeyByZXR1cm4gdGhpcy5faWR4OyB9XHJcbiAgcHVibGljIGdldFRpbGUoKSB7IHJldHVybiB0aGlzLl90aWxlOyB9XHJcbiAgcHVibGljIHZpZXdHZXRDbGllbnRXaWR0aCgpIHsgcmV0dXJuIHRoaXMuX2VsZW1lbnQuY2xpZW50V2lkdGg7IH1cclxuICBwdWJsaWMgdmlld0dldENsaWVudEhlaWdodCgpIHsgcmV0dXJuIHRoaXMuX2VsZW1lbnQuY2xpZW50SGVpZ2h0OyB9XHJcblxyXG4gIHB1YmxpYyBnZXRFbGVtZW50KCkgeyByZXR1cm4gdGhpcy5fZWxlbWVudDsgfVxyXG5cclxuICBwcml2YXRlIF9jcmVhdGVFbGVtZW50KCkge1xyXG4gICAgaWYgKHRoaXMuX2VsZW1lbnQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuX2VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZCgnc2xvdCcpO1xyXG4gICAgdGhpcy5fZWxlbWVudC5zdHlsZS53aWR0aCA9IGAke01hdGguZmxvb3IoMTAwIC8gU2xvdC5HcmlkU2l6ZSl9JWA7XHJcblxyXG4gICAgLy8gaW5zZXJ0IHRpbGVcclxuICAgIHRoaXMuX2VsZW1lbnQuaW5zZXJ0QmVmb3JlKHRoaXMuX3RpbGUuZ2V0RWxlbWVudCgpLCBudWxsKTtcclxuXHJcbiAgICAvLyBpbnNlcnQgc2xvdFxyXG4gICAgU2xvdC5FbEdyaWQuaW5zZXJ0QmVmb3JlKHRoaXMuX2VsZW1lbnQsIG51bGwpO1xyXG5cclxuICAgIHJldHVybjtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFNsb3Q7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vU3JjL1NjcmlwdHMvU2xvdC50cyIsImNsYXNzIFNjb3JlIHtcclxuICBwcml2YXRlIF9lbGVtZW50OiBIVE1MRWxlbWVudDtcclxuICBwcml2YXRlIF92YWx1ZTogbnVtYmVyID0gMDtcclxuXHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKHZhbHVlOiBudW1iZXIgPSAwKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gICAgdGhpcy5fZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzY29yZScpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGFkZCh2YWx1ZTogbnVtYmVyID0gMCkge1xyXG4gICAgdGhpcy5fdmFsdWUgKz0gdmFsdWU7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcmVzZXQoKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IDA7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdmlld1JlbmRlcigpIHtcclxuICAgIGlmICh0aGlzLl9lbGVtZW50KSB0aGlzLl9lbGVtZW50LmlubmVySFRNTCA9IHRoaXMuX3ZhbHVlLnRvU3RyaW5nKCk7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTY29yZTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9TcmMvU2NyaXB0cy9TY29yZS50cyJdLCJzb3VyY2VSb290IjoiIn0=